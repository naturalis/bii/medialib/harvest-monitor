SELECT
  he.ID,
  he.DATUM,
  he.MELDING,
  he.HARVESTSESSIONID,
  he.TIJD,
  hb.VOLGNUMMER
FROM
  FRM_harvesterrors he
INNER JOIN
  FRM_harvestsessions hs ON hs.ID = he.HARVESTSESSIONID
INNER JOIN
  FRM_harvestbronnen hb ON hb.ID = hs.HARVESTBRONID
WHERE
  cast(cast(datum AS datetime) + cast(tijd AS time) AS datetime) BETWEEN now() - INTERVAL 1 day AND now()
ORDER BY
  he.HARVESTSESSIONID;
