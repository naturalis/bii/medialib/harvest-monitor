#!/bin/sh

set -ex

SCRIPT_PATH="$(dirname "$(readlink -f $0)")"
ROOT="$(dirname "${SCRIPT_PATH}")"

docker compose pull
docker compose build --pull
docker compose up
