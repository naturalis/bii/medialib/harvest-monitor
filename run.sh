#!/bin/bash

set -e

ERROR_FILE="harvesterrors-current.tsv"

cd "${ROOT_DIR}"

# run query on database
mysql --login-path=crs --database "${CRS_DB_DBNAME}" -A < /app/query.sql > out.txt

# remove windows carriage return
tr -d '\15\32' < out.txt > "${ERROR_FILE}"

# Check for an empty file.
# MySQL does not print the column names when the query doesn't return any rows.
# But analyse-harvest-errors.R always expects a header line, so this is just a
# workaround to prevent the script from failing when the file is empty.
LINE_COUNT=$(wc -l "${ERROR_FILE}" | awk '{print $1}')

if [ "${LINE_COUNT}" -eq 0 ]
then
  echo -e "ID\tDATUM\tMELDING\tHARVESTSESSIONID\tTIJD\tVOLGNUMMER" > "${ERROR_FILE}"
fi

Rscript /app/analyse-harvest-errors.R
