#!/bin/sh

set -e

# Make the environment variables available in the cron job.
echo ROOT_DIR=\"$ROOT_DIR\" >> /etc/environment
echo CRS_DB_DBNAME=\"$CRS_DB_DBNAME\" >> /etc/environment

# Relay Shiny Server logs to its own stderr (which, by default,
# is saved to /var/log/shiny-server.log).
export SHINY_LOG_STDERR=1

# Run the monitor script once.
/app/run.sh

# Start the cron daemon.
cron

# Start the Shiny Server.
exec shiny-server
