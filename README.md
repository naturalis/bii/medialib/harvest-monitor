# harvest-monitor

Small web application for monitoring the medialib CRS harvest errors.

[![Build Status](https://travis-ci.org/naturalis/harvest-monitor.svg?branch=master)](https://travis-ci.org/naturalis/harvest-monitor)

## Development

First you need to [install Docker
Engine](https://docs.docker.com/engine/install/). Do not install "Docker
Desktop for Linux" on Linux because bind mounts don't work as expected there.

Select a Docker Compose override file depending on your target environment:

```bash
ln -s docker/overrides/docker-compose.dev.yml docker-compose.override.yml
```

Create an `.env` file for your environment variables:

```bash
cp .env.template .env
```

To run the harvest monitor on a server, you must provide the following
environment variables:

* `ROOT_DIR`: directory where data will be stored (harvesterrors-db.csv,
  harvesterrors-current.tsv, etc.).
* `CRS_DB_DBNAME`: the database name.

Authentication credentials are stored in an obfuscated login path file named
`.mylogin.cnf` (see [mysql_config_editor][1]). You need to create this file
once for your environment:

```bash
docker compose run --service-ports --rm harvest-monitor bash
mysql_config_editor set --login-path=crs --host=database --user=crs --port=3306 --password
```

**NOTE:** If you want to test this with the production database, you need to be
connected to eduVPN.

Enter the databse password when prompted. A file `.mylogin.cnf` will be
created in the users' home directory. Copy this file outside the Docker
container and inside this project's root directory:

```bash
docker cp <container>:/root/.mylogin.cnf .mylogin.cnf
```

For development you may use this pre-created login file:

```bash
cp .mylogin.dev.cnf .mylogin.cnf
```

The login file should be readable/writable only by current user:

```bash
chmod 0600 .mylogin.cnf
```

When running the container, store `.mylogin.cnf` in the home directory of the
user that needs access to the database. Using Docker Compose you can achieve
this by setting a volume as follows:

```yaml
volumes:
  - .mylogin.cnf:/root/.mylogin.cnf
```

You should then be able to login to the database as follows:

```bash
mysql --login-path=crs --database "${CRS_DB_DBNAME}"
```

The container can be started as follows:

```bash
./docker/start.sh
```

The development server should be up and running at
<http://localhost:3838/prod/>.

For development you may also add the following line to your hosts file to test
with the Let's Encrypt certificate (be sure to also set the correct AWS
credentials in `.env`):

```
127.0.0.1    crs-harvestmonitor.dryrun.link
```

You can then visit <https://crs-harvestmonitor.dryrun.link/prod/>.


## Checking logs

You can check this log file to verify the cron job is actually running:

```bash
docker compose exec harvest-monitor tail -F /var/log/cron.log
```

Shiny server logs can be checked with:

```bash
docker compose logs -f harvest-monitor
```


## Deployment

Deployment to production is done with Ansible, see
[ansible-harvest-monitor](https://gitlab.com/naturalis/bii/medialib/ansible-harvest-monitor).

[1]: https://dev.mysql.com/doc/refman/8.0/en/mysql-config-editor.html

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`
