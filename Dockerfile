FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ="Europe/Amsterdam"

RUN apt-get update && apt-get install -y --no-install-recommends \
    cron \
    file \
    lsb-release \
    mysql-client \
    r-base \
    r-cran-dt \
    r-cran-shiny \
    wget \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /root

RUN wget -q https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.17.973-amd64.deb \
  && apt-get install ./shiny-server-1.5.17.973-amd64.deb \
  && rm shiny-server-1.5.17.973-amd64.deb

COPY docker/shiny-server.conf /etc/shiny-server/shiny-server.conf
RUN rm -f /srv/shiny-server/index.html /srv/shiny-server/sample-apps

# Install crontab.
COPY crontab /etc/cron.d/harvest-monitor
RUN crontab /etc/cron.d/harvest-monitor

WORKDIR /app

COPY run.sh query.sql analyse-harvest-errors.R entrypoint.sh ./
RUN chmod 0744 run.sh entrypoint.sh

# Install R Shiny application.
COPY shiny/* /srv/shiny-server/prod/

EXPOSE 3838

CMD /app/entrypoint.sh
